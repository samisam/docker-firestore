# Docker + Firestore

A docker image that wraps the [Firebase Emulator](https://firebase.google.com/docs/rules/emulator-setup)


## Run

```
docker build . -t firestore:latest
docker run -it -p 8080:8080 firestore:latest
```

## Use it with docker-compose

```

version: '3.7'

services:
  app:
    build: .
    depends_on:
      - firestore
    volumes:
      - .:/app
      - node_modules:/app/node_modules
    environment:
      FIRESTORE_EMULATOR_HOST: firestore:8080

  firestore:
    image: registry.gitlab.com/samisam/docker-firestore

volumes:
  node_modules:
```