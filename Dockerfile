FROM openjdk:11-jre-slim

WORKDIR /firestore

RUN apt-get update && \
  apt-get install -y curl && \
  apt-get clean;
RUN curl -sL https://firebase.tools/bin/linux/latest -o /usr/local/bin/firebase
RUN chmod +x /usr/local/bin/firebase

RUN firebase setup:emulators:firestore

EXPOSE 8080

COPY firebase.json ./

CMD [ "firebase", "emulators:start", "--only", "firestore"]
